Feature: sample karate test script


  Background:
    * url 'https://stackoverflow.com/'

  Scenario: entering the key into the search bar and checking the results for compliance
    Given url 'https://stackoverflow.com'
    And param someKey = 'webdriver'
    When method get
    Then status 200



    Given url 'https://stackoverflow.com/search?q=webdriver'
    When method get
    Then status 200
    * def start = response.indexOf('flush-left js-search-results')
    * def end = response.indexOf('s-pagination pager fl')
    * def token = response.substring(start, end)

    * def start1 = token.indexOf('data-position="1"')
    * def end1 = token.indexOf('excerpt')
    * def fun1 = token.substring(start1, end1 - 50)
    * def start2 = token.indexOf('data-position="2"')
    * def end2 = token.indexOf('data-position="3"')
    * def fun2 = token.substring(start2, end2 - 50)
    * def start3 = token.indexOf('data-position="3"')
    * def end3 = token.indexOf('data-position="4"')
    * def fun3 = token.substring(start3, end3 - 50)
    * def start4 = token.indexOf('data-position="4"')
    * def end4 = token.indexOf('data-position="5"')
    * def fun4 = token.substring(start4, end4 - 50)
    * def start5 = token.indexOf('data-position="5"')
    * def end5 = token.indexOf('data-position="6"')
    * def fun5 = token.substring(start5, end5 - 50)
    * def start6 = token.indexOf('data-position="6"')
    * def end6 = token.indexOf('data-position="7"')
    * def fun6 = token.substring(start6, end6 - 50)
    * def start7 = token.indexOf('data-position="7"')
    * def end7 = token.indexOf('data-position="8"')
    * def fun7 = token.substring(start7, end7 - 50)
    * def start8 = token.indexOf('data-position="8"')
    * def end8 = token.indexOf('data-position="9"')
    * def fun8 = token.substring(start8, end8 - 50)
    * def start9 = token.indexOf('data-position="9"')
    * def end9 = token.indexOf('data-position="10"')
    * def fun9 = token.substring(start9, end9 - 50)
    * def start10 = token.indexOf('data-position="10"')
    * def end10 = token.indexOf('data-position="11"')
    * def fun10 = token.substring(start10, end10 - 50)
    * def start11 = token.indexOf('data-position="11"')
    * def end11 = token.indexOf('data-position="12"')
    * def fun11 = token.substring(start11, end11 - 50)
    * def start12 = token.indexOf('data-position="12"')
    * def end12 = token.indexOf('data-position="13"')
    * def fun12 = token.substring(start12, end12 - 50)
    * def start13 = token.indexOf('data-position="13"')
    * def end13 = token.indexOf('data-position="14"')
    * def fun13 = token.substring(start13, end13 - 50)
    * def start14 = token.indexOf('data-position="14"')
    * def end14 = token.indexOf('data-position="15"')
    * def fun14 = token.substring(start14, end14 - 50)
    * def start15 = token.indexOf('data-position="15"')
    * def fun15 = token.substring(start15)

    * match fun1 contains 'webdriver'
    * match fun2 contains 'webdriver'
    * match fun3 contains 'webdriver'
    * match fun4 contains 'webdriver'
    * match fun5 contains 'webdriver'
    * match fun6 contains 'webdriver'
    * match fun7 contains 'webdriver'
    * match fun8 contains 'webdriver'
    * match fun9 contains 'webdriver'
    * match fun10 contains 'webdriver'
    * match fun11 contains 'webdriver'
    * match fun12 contains 'webdriver'
    * match fun13 contains 'webdriver'
    * match fun14 contains 'WebDriver'
    * match fun15 contains 'webdriver'

  Scenario: discussion entry and title check
    Given driver 'https://stackoverflow.com/search?q=webdriver'
    And value('.question-hyperlink')
    And click('#question-summary-7263824')
    * waitUntil("document.readyState == 'complete'")
    When method get
    Then status 200


    Given url 'https://stackoverflow.com/questions/7263824'
    When method get
    Then status 200
    * def test = 'Get HTML source of WebElement in Selenium WebDriver using Python'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/3422262'
    When method get
    Then status 200
    * def test = 'How can I take a screenshot with Selenium WebDriver'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/10596417'
    When method get
    Then status 200
    * def test = 'Is there a way to get element by XPath using JavaScript in Selenium WebDriver'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/40208051'
    When method get
    Then status 200
    * def test = 'Selenium using Python - Geckodriver executable needs to be in PATH'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/6992993'
    When method get
    Then status 200
    * def test = 'Selenium C# WebDriver: Wait until element is present'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/12323403'
    When method get
    Then status 200
    * def test = 'How do I find an element that contains specific text in Selenium WebDriver (Python)'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/13724778'
    When method get
    Then status 200
    * def test = 'How to run Selenium WebDriver test cases in Chrome'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/34562061'
    When method get
    Then status 200
    * def test = 'WebDriver click() vs JavaScript click()'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/6509628'
    When method get
    Then status 200
    * def test = 'How to get HTTP Response Code using Selenium WebDriver'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/15058462'
    When method get
    Then status 200
    * def test = 'How to save and load cookies using Python + Selenium WebDriver'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/7991522'
    When method get
    Then status 200
    * def test = 'Test if element is present using Selenium WebDriver?'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/20986631'
    When method get
    Then status 200
    * def test = 'How can I scroll a web page using selenium webdriver in python?'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/8900073'
    When method get
    Then status 200
    * def test = 'Webdriver Screenshot'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/18539491'
    When method get
    Then status 200
    * def test = 'Headless Browser and scraping - solutions [closed]'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test


    Given url 'https://stackoverflow.com/questions/3189430'
    When method get
    Then status 200
    * def test = 'How to maximize the browser window in Selenium WebDriver (Selenium 2) using C#?'
    * def start = response.indexOf('question-header')
    * def end = response.indexOf('ml12 aside-cta grid--cell print:d-none sm:ml0 sm:mb12 sm:order-first sm:as-end')
    * def token = response.substring(start, end)
    * match token contains test






  Scenario: go to the tags
    Given driver 'https://stackoverflow.com/tags'
    And input('#tagfilter','webdriver')
    And click('{^}click')
    When method get
    Then status 200



    Given url 'https://stackoverflow.com/questions/tagged/webdriver'
    When method get
    Then status 200


  Scenario: checking for tag 'webdriver'
    Given url 'https://stackoverflow.com/questions/tagged/webdriver'
    When method get
    Then status 200
    * def start = response.indexOf('tags t-python')
    * def end = response.indexOf('browser-automation</a')
    * def token = response.substring(start, end)
    * match token contains 'webdriver'


